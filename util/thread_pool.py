#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Author:james.jiang@cootek.cn
import threading
import sys
if sys.version_info.major==2:
    import Queue as queue
    reload(sys)
    sys.setdefaultencoding("utf-8")
else:
    import queue as queue


class worker(threading.Thread):
    def __init__(self, thread_id, queue, func, *args, **kwargs):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.func_start = False
        self.finish = False
        self.running = True
        self.queue = queue
        self.mutex = threading.Lock()

    def set_status(self, status):
        self.running = status

    def set_finish(self, status):
        self.mutex.acquire()
        self.finish = status
        self.mutex.release()

    def set_content(self, func, args, kwargs):
        self.mutex.acquire()
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.func_start = True
        self.mutex.release()

    def run(self):
        while self.running:
            self.mutex.acquire()
            if self.func_start:
                self.func(*self.args, **self.kwargs)
                self.func_start = False
                #当前任务执行完后及时将该线程ID放入空闲队列, 以便后续任务继续复用
                self.queue.put(self.thread_id)
            self.running = not self.finish
            self.mutex.release()
            pass


class pool():
    """
        自定义的简单线程池, 可以在运行时动态添加任务, 而不是一开始
        就把所有任务都提交给线程池
    """
    def __init__(self, size):
        self.size = size
        self.workers = []
        self.idel_pos_queue = queue.Queue()
        self.prepare_queue = queue.Queue()
        self.mutex = threading.Lock()
        #初始化线程池, 一共有size个线程会在整个生命周期中运行
        for i in range(self.size):
            self.workers.append(worker(i, self.idel_pos_queue, None, None, None))
            self.workers[-1].start()
            #一开始所有线程都处于空运行状态, 全部置入空闲队列
            self.idel_pos_queue.put(i)

    def submit(self, func, *args, **kwargs):
        self.mutex.acquire()
        #如果所有线程都很忙, 那么新来的task暂时放入待执行队列 
        if self.idel_pos_queue.empty():
            self.prepare_queue.put((func, args, kwargs))
        else:
            worker = self.workers[self.idel_pos_queue.get()]
            #如果有新的线程空闲出来, 将优先执行待执行队列中的任务
            if self.prepare_queue.empty:
                worker.set_content(func, args, kwargs)
            else:
                body = self.prepare_queue.get()
                worker.set_content(body[0], body[1], body[2])
                self.prepare_queue.put((func, args, kwargs))
        self.mutex.release()

    def kill(self):
        """
            强行终止线程池
        """
        while not self.prepare_queue.empty():
            self.prepare_queue.get()
        for worker in self.workers:
            worker.set_finish(True)

    def stop(self):
        """
            正常结束线程池
        """
        #在生命周期快结束时, 如果待执行队列中还有
        #未执行完的任务, 那么继续将这些任务交给线
        #程池, 直到所有任务执行完毕
        while not self.prepare_queue.empty():
            while self.idel_pos_queue.empty():
                pass
            body = self.prepare_queue.get()
            worker = self.workers[self.idel_pos_queue.get()]
            worker.set_content(body[0], body[1], body[2])
        #设置任务结束标志, 这样可以保证每个线程在
        #自己的任务完全执行完后才终止自己
        for worker in self.workers:
            worker.set_finish(True)
