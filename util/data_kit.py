#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import mysql.connector
import redis
import os
import sys
import traceback
import pickle
import time
import commands
from general import *
crt_root = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append(crt_root)
from concurrent.futures import ThreadPoolExecutor
if sys.version_info.major==2:
    reload(sys)
    sys.setdefaultencoding("utf-8")

dbs = {}
executor = None

def get_data_source(data_type='mysql', config=None):
    """
        获取用于进行数据存储和读取操作的数据源, 该源类型可以
        是mysql, redis甚至文件IO或者内存地址 
    """
    if not config:
        config = init_config('%s/conf/config' % crt_root)['data_source_config'][data_type]
    datasource = None
    if data_type  in dbs:
        return dbs[data_type]
    try:
        if data_type == 'redis':
            host = config['host']
            port = config['port']
            db = config['db']
            datasource = redis.Redis(host=host, port=port, db=db)
        elif data_type == 'mysql':
            config = {
                'host' : config['host'],
                'port' : config['port'],
                'database' : config['database'],
                'user' : config['user'],
                'password' : config['password'],
                'charset' : config['charset'],
                'use_unicode' : True,
                'get_warnings' : False,
                'pool_size' : int(config['pool_size'])
            }
            datasource = mysql.connector.Connect(**config)
        elif data_type == 'local':
            f = open(config['path'])
            datasource = open(f, 'rb')
            f.close()
        dbs[data_type] = datasource
        return datasource
    except Exception as e:
        traceback.print_exc()


def delete_from_data_source(data_type='mysql', data={}, config=None):
    datasource = get_data_source(data_type, config)
    if data_type == 'mysql':
        cursor = datasource.cursor()
        delete = 'DELETE FROM %s WHERE %s' % (data['table'], data['condition'])
        cursor.execute(delete)
        datasource.commit()
        cursor.close()
    elif data_type == 'redis':
        datasource.hdel(data['name'], data['key'])
    elif data_type == 'local':
        pass
    else:
        pass

def query_from_data_source(data_type='mysql', query=None, config=None):
    datasource = get_data_source(data_type, config)
    rst = None
    if data_type == 'redis':
        rst = datasource.hget(query['name'], query['key'])
    elif data_type == 'local':
        rst = datasource
    elif data_type == 'mysql':
        cursor = datasource.cursor()
        cursor.execute(query)
        try:
            rst = cursor.fetchall()
            datasource.commit()
            cursor.close()
        except Exception as e:
            traceback.print_exc()
    else:
        pass
    return rst

def add_to_data_source(data_type='mysql', data={}, repeat=False, config=None, batch=False):
    table = None
    try:
        datasource = get_data_source(data_type, config)
        col = data['name']
        value = data['value']
        if data_type == 'mysql':
            table = data['table']
            cursor = datasource.cursor()
            insert_prefix = 'INSERT INTO ' if not repeat else 'REPLACE INTO '
            sql_insert = '%s %s (%s) VALUES (%s)' % (insert_prefix, table, ','.join(col), ','.join(['%s' for i in range(len(col))]))
            if batch:
                cursor.executemany(sql_insert, value)
            else:
                cursor.execute(sql_insert, value)
            datasource.commit()
            cursor.close()
        elif data_type == 'redis':
            datasource.hset(data['name'], data['key'], data['value'])
        elif data_type == 'local':
            pass
        else:
            pass
    except Exception as e:
        traceback.print_exc()


def echo_redis_flag(name, key):
    print(query_from_data_source('redis', {'name' : name, 'key' : key}))


def set_redis_flag(name, key, value):
    add_to_data_source('redis', {'name' : name, 'key' : key, 'value' : value})


if __name__ == '__main__':
    if sys.argv[1] == 'GET_REDIS_FLAG':
        echo_redis_flag(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == 'SET_REDIS_FLAG':
        set_redis_flag(sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        pass