source general.sh

export PYTHONPATH=util/:$PYTHONPATH
export PYTHONPATH=conf/:$PYTHONPATH

mode=$1
ref_date=$2
child_mode=$3

if [ $ref_date == "None" ]
then
    ref_date=`date +%Y%m%d -d "today 1 day ago"`
fi

spark="$spark_2_speculation --executor-memory 3g --queue root.pix --num-executors 100 --py-files general.py app_db.py $mode $child_mode"

# 从最新的app_install/usage中获取最新的包名
if [ $mode == "GET_NEW_PACKAGE" ]
then
    ref_date=`date +%Y%m%d -d "${ref_date} 1 day ago"`
    day_list="{"
    delay=0
    let tre=24
    while [ $delay -lt $tre ]
    do
        day=`date +%Y%m%d -d "$ref_date $delay day ago"`
        day_list=${day_list}${day}","
        let delay=delay+1
    done
    day_list=${day_list%?}
    day_list=${day_list}"}"
    $spark ${HDFS_PATH_ETL_USAGE}"/${day_list}" ${HDFS_PATH_ETL_INSTALL}"/${day_list}" $HDFS_PATH_APP_NEW 500
# 将最新的包名和已经爬取到的app_db信息进行映射, 找到待爬取的新包名 / 或者直接拉取老包名准备更新
elif [ $mode == "GENERATE_APP_TO_BE_CRAWLED" ]
then
    rm -r $LOCAL_PATH_APP_TO_BE_CRAWLED*
    if [ $child_mode == "NEW" ]
    then
        $spark $HDFS_PATH_APP_DB $LOCAL_PATH_APP_TO_BE_CRAWLED $HDFS_PATH_APP_NEW
    else
        $spark $HDFS_PATH_APP_DB $LOCAL_PATH_APP_TO_BE_CRAWLED
    fi
# 开启爬虫进行爬取
elif [ $mode == "CRAWL" ]
then
    rm -r $LOCAL_PATH_CRAWL_RST
    rm -r $LOCAL_PATH_CRAWL_ROOT/icons/*
    batch_num=`ls $LOCAL_PATH_APP_TO_BE_CRAWLED* | wc -l`
    python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play crt_batch_pos 0
    crt_batch_pos=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play crt_batch_pos`
    # 批量爬取(比如30万个app分割成10份, 每次爬取3万个app, 那么 batch_num=10)
    # 如果不分批, 直接爬取30万个app, 会导致scrapy运行时间过长, 内存占用陡增
    while [ $crt_batch_pos -ne $batch_num ]
    do
        rm -r $LOCAL_PATH_NOT_CRAWLED_APP
        rm -r $LOCAL_PATH_ERROR_APP
        LOCAL_PATH_CRAWL_RST_CRT=$LOCAL_PATH_CRAWL_RST"_"$crt_batch_pos
        rm -r $LOCAL_PATH_CRAWL_RST_CRT
        # 初始化爬虫完成标志
        python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play final_success "no"
        python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play crt_success "no"
        python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play crt_crawl_cnt 0
        # 开启爬虫
        cd app_db_crawl
        scrapy crawl appdata --loglevel=ERROR
        cd ..
        final_success=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play final_success`
        crt_success=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play crt_success`
        # 轮询爬虫是否完整结束
        # 完整结束是指: 每次爬取完后依然会有一些仍未爬到的app, 因此要对这些app进行二次爬取
        # 二次爬取过程会一直重复, 直到不存在仍未爬到的app, 或者二次爬取次数达到设定的上限
        # 这之后该次爬虫才算完整结束, 并设置 final_success 标志
        while [ $final_success == "no" ]
        do
            # 轮询爬虫的当前任务是否当前结束
            # 当前结束是指: 不管存不存在依然爬不到的app, 只要当前
            # 的爬虫任务结束了就设置 crt_success 标志, 如果有未爬
            # 取到的app, 将再次开启当前爬虫任务
            while [ $crt_success == "no" ]
            do
                sleep 15
                crt_success=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play crt_success`
            done
            final_success=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play final_success`
            # 当前爬虫任务结束了, 但是依然存在未爬取到的app, 或者
            # 二次爬取次数还未达到上限, 那么就将未爬取到的app重新
            # 设置为待爬取app, 进行二次爬取
            if [ $final_success == "no" ]
            then
                sleep 15
                crt_batch_pos=`python util/data_kit.py GET_REDIS_FLAG crawl_from_google_play crt_batch_pos`
                LOCAL_PATH_APP_TO_BE_CRAWLED_TRUE=$LOCAL_PATH_APP_TO_BE_CRAWLED"_"$crt_batch_pos
                rm -r $LOCAL_PATH_APP_TO_BE_CRAWLED_TRUE
                mv $LOCAL_PATH_NOT_CRAWLED_APP $LOCAL_PATH_APP_TO_BE_CRAWLED_TRUE
                rm -r $LOCAL_PATH_NOT_CRAWLED_APP
                python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play crt_success "no"
                # 当前爬虫任务有未爬完的app, 开启二次爬取爬虫任务
                cd app_db_crawl
                scrapy crawl appdata --loglevel=ERROR
                cd ..
            fi
        done
        # 当前批待爬取app已经爬取完成, 进行下一批app的爬取流程
        let crt_batch_pos=$crt_batch_pos+1
        python util/data_kit.py SET_REDIS_FLAG crawl_from_google_play crt_batch_pos $crt_batch_pos
    done
    LOCAL_PATH_CRAWL_RST_ALL=$LOCAL_PATH_CRAWL_RST"_*"
    cat $LOCAL_PATH_CRAWL_RST_ALL > $LOCAL_PATH_CRAWL_RST
# 将爬取到的结果(json格式)处理并存入hdfs
elif [ $mode == "MERGE_TO_HDFS" ]
then
    $spark $LOCAL_PATH_CRAWL_RST $HDFS_PATH_APP_DB $HDFS_PATH_ROOT
elif [ $mode == "MERGE_TO_MYSQL" ]
then
    $spark $HDFS_PATH_APP_DB
else
    exit 1
fi
