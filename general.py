#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    import pyspark
except:
    pass
import sys, os, ujson, hashlib, subprocess, commands
common_root = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
if sys.version_info.major==2:
    import urllib2 as req
else:
    import urllib.request as req

if sys.version_info.major==2:
    reload(sys)
    sys.setdefaultencoding("utf-8")

HADOOP = '/usr/local/hadoop-2.6.3/bin/hadoop'


def check_hdfs_status(path, status):
    """
        检查hdfs上对应文件是否写入成功
    """
    rst = subprocess.check_output('%s fs -ls %s/_SUCCESS | wc -l' % (HADOOP, path), shell=True)
    return int(rst) == int(status)


def hdfs_delete(path):
    """
        删除hdfs上的文件
    """
    subprocess.call('%s fs -rm -r %s' % (HADOOP, path), shell=True)


def hdfs_move(path_old, path_new):
    """
        移动hdfs上的文件
    """
    subprocess.call('%s fs -mv %s %s' % (HADOOP, path_old, path_new), shell=True)


def hdfs_transfer(path_hdfs, path_local, mode='TO_LOCAL'):
    """
        在本地和集群之间传输文件
    """
    if mode == 'TO_LOCAL':
        subprocess.call('%s fs -copyToLocal %s %s' % (HADOOP, path_hdfs, path_local), shell=True)
    elif mode == 'FROM_LOCAL':
        subprocess.call('%s fs -copyFromLocal %s %s' % (HADOOP, path_local, path_hdfs), shell=True)


def app_name_persist(path, df_to_be_crawled, collect_limit=15000):
    """
        读取hdfs上的待爬取包名, 并批量collect到本地
    """
    app_to_be_crawled_cnt = df_to_be_crawled.count()
    print("Num of apps that to be crawled: %s" % str(app_to_be_crawled_cnt))
    subprocess.call('rm -r %s*' % path, shell=True)
    if app_to_be_crawled_cnt > collect_limit:
        split_num = int(app_to_be_crawled_cnt / collect_limit)
        dfs = df_to_be_crawled.randomSplit([1.0 / split_num for i in range(split_num)], 1)
    else:
        dfs = [df_to_be_crawled]
    for i in range(len(dfs)):
        subprocess.call('rm -r %s' % ('%s_%s' % (path, i)), shell=True)
        with open('%s_%s' % (path, i), 'a+') as f:
            for package_name in [item['package_name'] for item in dfs[i].select('package_name').collect()]:
                f.write(package_name + '\n')


def init_config(config_file):
    """
        载入配置文件并解析
    """
    config = {}
    if sys.version_info.major==2:
        execfile(config_file, config)
    else:
        with open(config_file, 'r') as f:
            exec(f.read(), config)
    return config


def init_context(app_name, compress_mode='gzip'):
    """
        初始化Spark上下文
    """
    try:
        conf = pyspark.SparkConf().setAppName(app_name)
        sc = pyspark.SparkContext(conf = conf)
        sc.setLogLevel('WARN')
        sqc = pyspark.sql.SQLContext(sc)
        sqc.setConf('spark.sql.parquet.compression.codec', compress_mode)
        sqc.setConf('spark.python.worker.reuse', True)
        #sqc.setConf('spark.driver.extraClassPath', 'jvm/libs/mysql-connector-java-5.1.45-bin.jar')
        #sqc.setConf('spark.executor.extraClassPath', 'jvm/libs/mysql-connector-java-5.1.45-bin.jar')
        return sc, sqc
    except:
        return None, None


def str_2_md5(input):
    """
        字符串转md5
    """
    if type(input) not in (str, unicode):
        return ''
    m2 = hashlib.md5()   
    m2.update(input)
    return m2.hexdigest() 


def get_files(path, list_name):
    """
        获取某个目录里的所有文件名
    """
    for file in os.listdir(path):  
        file_path = os.path.join(path, file)  
        if os.path.isdir(file_path):  
            listdir(file_path, list_name)
        else:  
            list_name.append(file_path)


def json_filter(x):
    """
        json解析
    """
    try:
        return ujson.loads(x.strip(',').strip('\n').replace('\\', '').replace("'", ''))
    except:
        return None


def not_family(x):
    return (x != 'Ages 5 & Under' and \
            x != 'Ages 6-8' and \
            x != 'Ages 9 & Up' and \
            x != 'Creativity' and \
            x != 'Education' and \
            x != 'Brain Games' and \
            x != 'Music & Video' and \
            x != 'Action & Adventure' and \
            x != 'Pretend Play')
    

def family(x):
    return (x == 'Ages 5 & Under' or \
            x == 'Ages 6-8' or \
            x == 'Ages 9 & Up' or \
            x == 'Creativity' or \
            x == 'Education' or \
            x == 'Brain Games' or \
            x == 'Music & Video' or \
            x == 'Action & Adventure' or \
            x == 'Pretend Play')


def game(x):
    return (x == 'Strategy' or \
            x == 'Action' or \
            x == 'Casino' or \
            x == 'Board' or \
            x == 'Role Playing' or \
            x == 'Educational' or \
            x == 'Arcade' or \
            x == 'Racing' or \
            x == 'Card' or \
            x == 'Adventure' or \
            x == 'Simulation' or \
            x == 'Sports' or \
            x == 'Word' or \
            x == 'Casual' or \
            x == 'Puzzle' or \
            x == 'Music' or \
            x == 'Trivia')


def ctg_process_fun(x, href):
    if len(x) > 1:
        cnt = 0
        for v in x:
            if not_family(v):
                if v == 'Sports':
                    return (v + '[GAME]') if 'GAME' in href else (v + '[APPLICATION]')
                else:
                    if game(v):
                        return v + '[GAME]'
                    else:
                        return v + '[APPLICATION]'
                break
            else:
                cnt += 1
        if cnt == len(x):
            return x[0] + '[GAME]'
    else:
        if len(x) == 1:
            v = x[0]
            if v == 'Sports':
                return (v + '[GAME]') if 'GAME' in href else (v + '[APPLICATION]')
            else:
                if game(v) or family(v):
                    return v + '[GAME]'
                else:
                    return v + '[APPLICATION]'
        return 'Unknown'


def add_package(path, set_data):
    with open(path, 'r') as f:
        for line in f:
            line = line.replace('\n', '').replace('\r', '').replace('\n\r', '').replace('\r\n', '')
            if line != '':
                set_data.add(line)

def str_2_num(x):
    x = x.replace('"', '').replace("'", '').replace('+', '').replace('-', '').replace(',', '')
    return x
