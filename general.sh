function file_ready {
filepath=$1
num=`ls $filepath | wc -l`
if [ $num -ge 1 ]
then
    return 0
else
    return 1
fi
}

function wait_file_ready {
filepath=$1
file_ready $1
while [ $? -ne 0 ]
do
    echo "$filepath not ready"
    sleep 30
    file_ready $filepath
done
}

function get_last_monday {
    in_date=$1
    if [ ! $in_date ]; then  
      in_date=$(date +%Y%m%d)  
    fi
    statday=`date -d "$in_date -1 weeks" +%Y%m%d`
    whichday=$(date -d $statday +%w)
    if [ $whichday == 0 ]; then  
      startday=`date -d "$statday -6 days" +%Y%m%d`  
    else  
      startday=`date -d "$statday -$[${whichday} - 1] days" +%Y%m%d`  
    fi
    echo $startday
}

function get_last_month {
    in_date=$1
    echo `date -d "$in_date -1 months" +%Y%m`
}

hdfs_user=james.jiang
local_user=james.jiang

spark_2_speculation='/usr/local/spark-2.1.1-bin-hadoop2.6/bin/spark-submit --master yarn-client --driver-memory 2G --conf spark.speculation.interval=30s --conf spark.speculation=true'

# 用于存放爬虫结果的本地目录
LOCAL_PATH_CRAWL_ROOT=/home/$local_user/app_db_crawl

# 待爬取的包名
LOCAL_PATH_APP_TO_BE_CRAWLED=$LOCAL_PATH_CRAWL_ROOT/app_to_be_crawled
# 爬虫结束后仍未爬到的包名(准备二次爬取)
LOCAL_PATH_NOT_CRAWLED_APP=$LOCAL_PATH_CRAWL_ROOT/app_not_crawled
# 爬虫得到的结果
LOCAL_PATH_CRAWL_RST=$LOCAL_PATH_CRAWL_ROOT/crawl_rst.json
# emmmm
LOCAL_PATH_ERROR_APP=$LOCAL_PATH_CRAWL_ROOT/app_error_crawled
# emmmm
HDFS_PATH_APP_NEW=/user/$hdfs_user/app_name_new
# emmmm
HDFS_PATH_ROOT=/user/$hdfs_user

HDFS_PATH_TRENDS_ROOT=/user/dw/trends
HDFS_PATH_ETL_USAGE=$HDFS_PATH_TRENDS_ROOT/etl/app_usage/{ap,eu,us}
HDFS_PATH_ETL_INSTALL=$HDFS_PATH_TRENDS_ROOT/etl/app_install/{ap,eu,us}
HDFS_PATH_APP_DB=$HDFS_PATH_TRENDS_ROOT/app_db/latest

if [ $1 == "GET_CRAWL_CONFIG" ]
then
    echo ${LOCAL_PATH_CRAWL_ROOT}":"${LOCAL_PATH_APP_TO_BE_CRAWLED}":"${LOCAL_PATH_NOT_CRAWLED_APP}":"${LOCAL_PATH_CRAWL_RST}":"${LOCAL_PATH_ERROR_APP}
else
    kinit -kt /home/$local_user/$local_user.keytab $local_user
    ls $LOCAL_PATH_CRAWL_ROOT
    if [ $? -ne 0 ]
    then
        mkdir -p $LOCAL_PATH_CRAWL_ROOT
        ls $LOCAL_PATH_CRAWL_ROOT/flag
        if [ $? -ne 0 ]
        then
            mkdir -p $LOCAL_PATH_CRAWL_ROOT/flag
        fi
        ls $LOCAL_PATH_CRAWL_ROOT/icons
        if [ $? -ne 0 ]
        then
            mkdir -p $LOCAL_PATH_CRAWL_ROOT/icons
        fi
    fi
fi
