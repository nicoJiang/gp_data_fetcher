#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import sys
import os
import time
import subprocess
import traceback
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../../../")))
import pickle
import traceback
from scrapy.exceptions import CloseSpider
from appdata.items import *
from scrapy.spiders import CrawlSpider, Rule
from appdata.settings import *
from general import *
from util.data_kit import *
if sys.version_info.major==2:
    from urlparse import urlparse
    from urlparse import parse_qs
    reload(sys)
    sys.setdefaultencoding("utf-8")
else:
    from urllib.parse import urlparse
    from urllib.parse import parse_qs


def init_app_to_be_crawled():
    """
        初始化待爬取包名
    """
    try:
        print('我他喵的到底被初始化了几次?')
        array_app_to_be_crawled = []
        for line in dict_crawl_file['app_to_be_crawled']:
            array_app_to_be_crawled.append(target_generate(line.replace('\n', '').replace('\r', '').replace('\r\n', '')))
        dict_debug['total_cnt'] = len(array_app_to_be_crawled)
        return array_app_to_be_crawled
    except Exception as e:
        for key in thread_pools:
            thread_pools[key].kill()
        traceback.print_exc()
        raise CloseSpider('GP_SPIDER[init_app_to_be_crawled]')


class GpSpider(scrapy.Spider):
    name = BOT_NAME
    allowed_domains = [TARGET_HOST]
    start_urls = init_app_to_be_crawled()[:]
    def parse(self, response):
        try:
            global dict_crawl_file
            item = AppdataItem()
            dict_debug['crt_process_cnt'] += 1
            r = urlparse(response.url)
            params = parse_qs(r.query, True)
            package_name = ','.join(params['id']).replace('\n', '').replace(' ', '').replace(',', '')
            parse_item_from_source(item, package_name, response)
            if invalid_check(item):
                dict_debug['crawled_cnt'] += 1
                global dict_crawl_file
                #正常爬取到的信息将以json格式组织在本地的json文件中
                if dict_crawl_file['crawl_rst'] is not None:
                    #区分[系统, 预装]等类型的app
                    app_type = item_type_define(package_name, '0')
                    dict_crawl_file['crawl_rst'].write(generate_item(package_name, app_type, [item['install_count'], \
                                                                                              item['score'], \
                                                                                              item['company'].replace('"', '-').replace("'", '-').replace(',', '-').replace(':', '-'), \
                                                                                              item['category'], \
                                                                                              item['app_name'].replace('"', '-').replace("'", '-').replace(',', '-').replace(':', '-'), \
                                                                                              item['icon_url'], \
                                                                                              item['rank_count'], \
                                                                                              item['read_more']]))
            # 爬取失败的包名将进行二次爬取
            else:
                dict_debug['no_crawled_cnt'] += 1
                dict_crawl_file['app_not_crawled'].write('%s\n' % package_name)
            time_crt = int(time.time())
            if time_crt - dict_debug['time_init'] > 30:
                print(dict_debug)
                print([item['package_name'], item['install_count'], item['score'], item['company'], item['category'], item['app_name'], item['rank_count']])
                dict_debug['time_init'] = time_crt
                print(dict_debug)
            yield item
        except Exception as e:
            print('**********************gp_spider**********************')
            traceback.print_exc()
            task_kill()
        

