#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import commands
import sys
import traceback
import os
import time
crt_root = os.path.join(os.path.dirname(__file__))
crt_root = crt_root.split('/')
crt_root.remove(crt_root[-1])
crt_root.remove(crt_root[-1])
crt_root = '/'.join(crt_root)
sys.path.append(os.path.abspath(crt_root))
from general import *
from util.thread_pool import pool
from util.data_kit import *
if sys.version_info.major==2:
    reload(sys)
    sys.setdefaultencoding("utf-8")
    FNULL = open(os.devnull, 'w')

# Scrapy settings for appdata project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'appdata'

SPIDER_MODULES = ['appdata.spiders']

NEWSPIDER_MODULE = 'appdata.spiders'

TARGET_HOST = 'play.google.com'

def target_generate(data):
    return "https://play.google.com/store/apps/details?id=" + data.strip() + "&hl=en"

LOCAL_PATHS = subprocess.check_output('bash %s/general.sh GET_CRAWL_CONFIG' % crt_root, shell=True).replace('\n', '').split(':')

LOCAL_PATH_CRAWL_ROOT = LOCAL_PATHS[0]
LOCAL_PATH_APP_TO_BE_CRAWLED = LOCAL_PATHS[1]
LOCAL_PATH_NOT_CRAWLED_APP = LOCAL_PATHS[2]
LOCAL_PATH_CRAWL_RST = LOCAL_PATHS[3]
LOCAL_PATH_ERROR_APP = LOCAL_PATHS[4]

dict_debug = {
    'crt_process_cnt' : 0,
    'crawled_cnt' : 0,
    'no_crawled_cnt' : 0,
    'err_crawled_cnt' : 0,
    'time_init' : int(time.time()),
    'total_cnt' : 0
}

dict_crawl_file = {
    'crawl_rst' : None, # 存放爬取结果的json文件
    'app_to_be_crawled' : None, # 存放待爬取app列表
    'app_not_crawled' : None, # 存放仍未爬取到的app列表
    'app_err_crawled' : None, # 存放爬取出错的app列表
    'success_flag' : None, # 最终爬取成功的标志文件
    'success_flag_crt' : None, # 当前任务爬取成功标志文件
    'repeat_crawl_cnt' : None, # 记录重复爬取次数的文件
    'batch_crawl_cnt' : None # 记录批量爬取次数的文件
}

RE_CRAWL_CNT_LIMIT = 10

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'appdata (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 64
CONCURRENT_ITEMS = 256

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 128
CONCURRENT_REQUESTS_PER_IP = 128

# Disable cookies (enabled by default)
COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'appdata.middlewares.AppdataSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
   'appdata.middlewares.AppdataDownloaderMiddleware': 543,
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
EXTENSIONS = {
   'scrapy.extensions.telnet.TelnetConsole': None,
   'appdata.middlewares.GateMiddleware' : 500
}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'appdata.pipelines.ImagePipeline': 300,
}

IMAGES_STORE = '%s/icons' % LOCAL_PATH_CRAWL_ROOT

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

UNKNOWN_APP_REF = {
    'SYS' : set(),
    'PRE' : set(),
    'GOOGLE' : set()
}

thread_pools = {
    'thread_pool_image' : pool(5)
}

try:
    add_package('%s/conf/sp_app/android.txt' % crt_root, UNKNOWN_APP_REF['SYS'])
    add_package('%s/conf/sp_app/google.txt' % crt_root, UNKNOWN_APP_REF['GOOGLE'])
    pre_files = []
    get_files('%s/conf/sp_app/pre' % crt_root, pre_files)
    for path in pre_files:
        add_package(path, UNKNOWN_APP_REF['PRE'])
except:
    traceback.print_exc()

MY_USER_AGENT = [
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)",
    "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.5; AOLBuild 4337.35; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)",
    "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322)",
    "Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.3 (Change: 287 c9dfb30)",
    "Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.6",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2pre) Gecko/20070215 K-Ninja/2.1.1",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9) Gecko/20080705 Firefox/3.0 Kapiko/3.0",
    "Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Fedora/1.9.0.8-1.fc10 Kazehakase/0.5.6",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20",
    "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.11 TaoBrowser/2.0 Safari/536.11",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; LBBROWSER)",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E; LBBROWSER)",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 LBBROWSER",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; QQBrowser/7.0.3698.400)",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; 360SE)",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)",
    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1",
    "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b13pre) Gecko/20110307 Firefox/4.0b13pre",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
    "Mozilla/5.0 (X11; U; Linux x86_64; zh-CN; rv:1.9.2.10) Gecko/20100922 Ubuntu/10.10 (maverick) Firefox/3.6.10",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
]


def task_kill():
    pid = os.getpid()
    ppid = os.getppid()
    for key in thread_pools:
        thread_pools[key].kill()
    subprocess.call("kill -9 %s" % pid, shell=True)


# 爬虫初始化操作本应放在 spider_open 函数中
# 但实际测试发现, 由于信号量的延迟, spider_open 会
# 在爬虫开启之后才执行, 导致出错 
if dict_crawl_file['app_to_be_crawled'] is None:
    try:
        global dict_crawl_file
        dict_crawl_file['app_not_crawled'] = open(LOCAL_PATH_NOT_CRAWLED_APP, 'a+')
        dict_crawl_file['app_err_crawled'] = open(LOCAL_PATH_ERROR_APP, 'a+')
        batch_pos = query_from_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_batch_pos'})
        dict_crawl_file['app_to_be_crawled'] = open('%s_%s' % (LOCAL_PATH_APP_TO_BE_CRAWLED, batch_pos), 'r')
        dict_crawl_file['crawl_rst'] = open('%s_%s' % (LOCAL_PATH_CRAWL_RST, batch_pos), 'a+')
        add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'final_success', 'value' : 'no'})
        add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_success', 'value' : 'no'})
    except Exception as e:
        traceback.print_exc()
        task_kill()