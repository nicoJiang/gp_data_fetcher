#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

import pickle
import subprocess
import traceback
import random
import os, sys
from fake_useragent import UserAgent
from scrapy import signals
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from settings import LOCAL_PATH_CRAWL_ROOT, \
                     LOCAL_PATH_APP_TO_BE_CRAWLED, \
                     LOCAL_PATH_NOT_CRAWLED_APP, \
                     LOCAL_PATH_CRAWL_RST, \
                     RE_CRAWL_CNT_LIMIT, \
                     UNKNOWN_APP_REF, \
                     thread_pools, \
                     dict_debug, \
                     dict_crawl_file, \
                     task_kill
from scrapy.exceptions import CloseSpider
from items import *
crt_root = os.path.join(os.path.dirname(__file__))
crt_root = crt_root.split('/')
crt_root.remove(crt_root[-1])
crt_root.remove(crt_root[-1])
crt_root = '/'.join(crt_root)
sys.path.append(os.path.abspath(crt_root))
from general import *
from util.data_kit import *
try:
    if sys.version_info.major==2:
        reload(sys)
        sys.setdefaultencoding("utf-8")
except:
    pass

class AppdataSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r


class AppdataDownloaderMiddleware(UserAgentMiddleware):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    # def __init__(self, user_agent):
    #     self.user_agent = user_agent

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        try:
            s = cls(user_agent=crawler.settings.get('MY_USER_AGENT'))
            crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
            return s
        except Exception as e:
            print('************AppdataDownloaderMiddleware[from_crawler]************')
            traceback.print_exc()
            task_kill()

    def spider_opened(self, spider):
        """
            爬虫初始化(构造)
        """
        spider.logger.info('Spider opened: %s' % spider.name)

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        agent = random.choice(self.user_agent)
        request.headers['User-Agent'] = agent
        return None

    def process_response(self, request, response, spider):

        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        try:
            # 记录仍未爬取到的包名
            # 如果该包名性质符合(系统, 预装)等, 则将其写入已爬取的json文件中
            if int(response.status) >= 400:
                dict_debug['err_crawled_cnt'] += 1
                rst = response.url.split('=')
                dict_debug['failure_package_name'] = rst
                app_type = '4'
                if len(rst) > 1:
                    package_name = rst[1].split('&')[0]
                    app_type = item_type_define(package_name, '4')
                    if int(response.status) == 404:
                        dict_crawl_file['app_err_crawled'].write('%s:%s\n' % (package_name, str(response.status)))
                    else:
                        dict_crawl_file['app_not_crawled'].write('%s:%s\n' % (package_name, str(response.status)))
                    if app_type != '4':
                        dict_crawl_file['crawl_rst'].write(generate_item(package_name, app_type, None))
                else:
                    dict_debug['failure_package_name'] = rst
        except Exception as e:
            print('************AppdataDownloaderMiddleware[process_response]************')
            traceback.print_exc()
            task_kill()
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass


class GateMiddleware(object):

    def __init__(self):
        pass

    @classmethod
    def from_crawler(cls, crawler):
        try:
            s = cls()
            crawler.signals.connect(s.spider_closed, signal=signals.spider_closed)
            crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
            return s
        except Exception as e:
            print('************GateMiddleware[from_crawler]************')
            traceback.print_exc()
            task_kill()

    def spider_opened(self, spider):
        """
            爬虫初始化(构造)
        """
        spider.logger.info('Spider opened: %s' % spider.name)


    def spider_closed(self, spider):
        """
            爬虫结束(析构)
        """
        try:
            global dict_crawl_file, thread_pools
            dict_crawl_file['app_to_be_crawled'].close()
            dict_crawl_file['app_not_crawled'].close()
            dict_crawl_file['app_err_crawled'].close()
            dict_crawl_file['crawl_rst'].close()
            for key in thread_pools:
                thread_pools[key].stop()
            not_crawled_app_num = int(subprocess.check_output('cat %s | wc -l' % LOCAL_PATH_NOT_CRAWLED_APP, shell=True))
            re_crawl_cnt = int(query_from_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_crawl_cnt'}))
            # 判断是否需要二次爬取, 并最终写入爬取成功标志
            if not_crawled_app_num <= 1 or re_crawl_cnt >= 10:
                add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'final_success', 'value' : 'yes'})
                add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_crawl_cnt', 'value' : 0})
            else:
                re_crawl_cnt = int(query_from_data_source('redis', 'crawl_from_google_play', 'crt_crawl_cnt'))
                add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_crawl_cnt', 'value' : re_crawl_cnt + 1})
            add_to_data_source('redis', {'name' : 'crawl_from_google_play', 'key' : 'crt_success', 'value' : 'yes'})
        except Exception as e:
            print('************GateMiddleware[spider_closed]************')
            traceback.print_exc()
            task_kill()
