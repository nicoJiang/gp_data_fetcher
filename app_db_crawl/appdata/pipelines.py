#encoding=utf-8
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import commands
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.contrib.pipeline.images import ImagesPipeline
from PIL import Image
import sys, os
crt_root = os.path.join(os.path.dirname(__file__))
crt_root = crt_root.split('/')
crt_root.remove(crt_root[-1])
crt_root.remove(crt_root[-1])
crt_root = '/'.join(crt_root)
sys.path.append(os.path.abspath(crt_root))
from general import *
from settings import thread_pools, IMAGES_STORE, FNULL
if sys.version_info.major==2:
    reload(sys)
    sys.setdefaultencoding("utf-8")
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

class AppdataPipeline(object):
    def process_item(self, item, spider):
        return item

class ImagePipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        yield Request(item['icon_url'])

    def __image_operation(self, path_post, package_name):
        """
            对下载完成的图片进行处理(重命名, 压缩)
        """
        src = '%s/%s' % (IMAGES_STORE, path_post)
        dst = '%s/%s' % (IMAGES_STORE, md5(package_name.strip().strip(' ').strip(',')))
        subprocess.call("rm -r %s.png" % dst, shell=True, stdout=FNULL, stderr=FNULL)
        subprocess.call("mv %s %s.png" % (src, dst), shell=True)
        sImg=Image.open(dst + '.png')
        dImg=sImg.resize((45, 45), Image.ANTIALIAS)
        dImg.save(dst + '_resize.png')
        subprocess.call("rm -r %s.png" % dst, shell=True)
        subprocess.call("mv %s_resize.png %s.png" % (dst, dst), shell=True)

    def item_completed(self, results, item, info):
        """
            每次成功下载一张图片, 就将后续的图片处理任务提交给线程池
        """
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem("Item contains no images")
        else:
            thread_pools['thread_pool_image'].submit(self.__image_operation, image_paths[0], item['package_name'])
        return item

    #Name download version
    def file_path(self, request, response=None, info=None):
        image_guid = request.url.split('/')[-1]
        return '%s' % (image_guid)

    #Name thumbnail version
    def thumb_path(self, request, thumb_id, response=None, info=None):
        image_guid = thumb_id + response.url.split('/')[-1]
        return 'thumbs/%s/%s.jpg' % (thumb_id, image_guid)