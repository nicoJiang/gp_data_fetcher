# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy, os, sys
from settings import UNKNOWN_APP_REF, task_kill
crt_root = os.path.join(os.path.dirname(__file__))
crt_root = crt_root.split('/')
crt_root.remove(crt_root[-1])
crt_root.remove(crt_root[-1])
crt_root = '/'.join(crt_root)
sys.path.append(os.path.abspath(crt_root))
from general import ctg_process_fun


URL_ANDROID_DEFAULT_ICON=''

class AppdataItem(scrapy.Item):
    # define the fields for your item here like:

    package_name = scrapy.Field()
    install_count = scrapy.Field()
    score = scrapy.Field()
    rank_count = scrapy.Field()
    company = scrapy.Field()
    category = scrapy.Field()
    app_name = scrapy.Field()
    icon_url = scrapy.Field()
    read_more = scrapy.Field()


def generate_item(package_name, app_type, array_content):
    value = tuple([package_name] + array_content + [app_type]) if \
            array_content and len(array_content) == 8 else \
            tuple([package_name] + ['Unknown' for i in range(8)] + [app_type])
    return '{"package_name":"%s", \
             "install_count":"%s", \
             "score":"%s", \
             "company":"%s", \
             "category":"%s", \
             "app_name":"%s", \
             "icon_url":"%s", \
             "rank_count":"%s", \
             "read_more":"%s", \
             "app_type":"%s"}\n' % value


def parse_item_from_source(item, package_name, response):
    item['package_name'] = package_name
    item['icon_url'] = [v for v in response.xpath("//img[@itemprop='image']").xpath("@src").extract()]
    item['icon_url'] = item['icon_url'][0] if len(item['icon_url']) > 0 else URL_ANDROID_DEFAULT_ICON
    item['score'] =  '|'.join(response.xpath("//div[@class='BHMmbe']").xpath("text()").extract()).strip()
    item['score'] = '0' if item['score'] == '' else item['score']
    item['category'] =  response.xpath("//a[@itemprop='genre']").xpath("text()").extract()
    ctg_href = ','.join(response.xpath("//a[@class='document-subtitle category']").xpath("@href").extract())
    item['category'] = ctg_process_fun(item['category'], ctg_href)
    item['read_more'] = '|'.join(response.xpath("//div[@jsname='sngebd']").xpath("text()").extract())

    install_count_path = response.xpath("//span[@class='htlgb']")
    if len(install_count_path) > 2:
        item['install_count'] =  ','.join(install_count_path[5].xpath("text()").extract()).strip()
        item['install_count'] = item['install_count'].replace(',', '').replace('+', '')
        if not item['install_count'].isdigit():
            item['install_count'] = ','.join(install_count_path[3].xpath("text()").extract()).strip()
        item['install_count'] = item['install_count'].replace(',', '').replace('+', '')

    rank_count_path = response.xpath("//span[@class='AYi5wd TBRnV']/span")
    if len(rank_count_path) > 0:
        item['rank_count'] =  ','.join(rank_count_path[0].xpath("text()").extract()).strip()
        item['rank_count'] = item['rank_count'].replace('.', '').replace(',', '').replace('+', '')
    else:
        item['rank_count'] = '0'

    company_path = response.xpath("//a[@class='hrTbp R8zArc']")
    if len(company_path) > 0:
        item['company'] =  ','.join(company_path[0].xpath("text()").extract()).strip()
    else:
        item['company'] = ''

    app_name_path = response.xpath("//h1[@itemprop='name']/span")
    if len(app_name_path) > 0:
        item['app_name'] = ','.join(app_name_path[0].xpath("text()").extract()).strip()
    else:
        item['app_name'] = ''
    
    try:
        if not item['install_count'].isdigit() or not item['rank_count'].isdigit() or not item['score'].replace('.', '').isdigit():
            item['install_count'] = 0
            item['rank_count'] = 0
            item['score'] = 0
            print([item['package_name'], item['install_count'], item['rank_count'], item['score']], "WIRED HTML FORMAT!")
        else:
            item['install_count'] = int(item['install_count'])
            item['rank_count'] = int(item['rank_count'])
            item['score'] = float(item['score'])
    except:
        print(item)


def item_type_define(package_name, type_init):
    app_type = type_init
    if package_name[0:12]=='com.android.':
        app_type = '1'
    elif package_name[0:11]=='com.google.':
        app_type = '2'
    elif package_name in UNKNOWN_APP_REF['PRE']:
        app_type = '3'
    else:
        pass
    return app_type


def invalid_check(item):
    return item['app_name'] != ''
