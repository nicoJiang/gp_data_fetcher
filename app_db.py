#/bin/env python
#coding:utf-8
import time
import pickle
import sys, os
import commands, time, subprocess
import datetime
import traceback
import io
import ujson
crt_root = os.path.abspath(os.path.join(os.path.dirname(__file__)))
crt_root = crt_root.split('/')[:-1]
crt_root = '/'.join(crt_root) + '/app_db'
from general import *
from util.data_kit import *
try:
    import pyspark
    from pyspark.sql.functions import *
    from pyspark.sql.types import Row, IntegerType, ArrayType, StringType
    reload(sys)
    sys.setdefaultencoding('utf-8')
    sys.path.append('.')
except:
    pass


def get_new_app(sqc, usage_path, install_path, out_path, app_user_num_limit):
    """
        从最近的app_install/usage的etl结果中获取所有候选app包名
    """
    df_usage = sqc.read.parquet(usage_path).select('use_app_name', 'identifier') \
                                           .withColumnRenamed('use_app_name', 'package_name')
    df_install = sqc.read.parquet(install_path).select('use_app_name', 'identifier') \
                                               .withColumnRenamed('use_app_name', 'package_name')
    df_new = df_usage.union(df_install).dropDuplicates()
    df_new = df_new.groupBy('package_name').agg(countDistinct('identifier').alias('user_num')) \
                                           .filter("user_num > " + str(app_user_num_limit)).select('package_name')
    df_new.repartition(5).write.mode('overwrite').parquet(out_path)


def generate_app_to_be_crawled(sqc, child_mode, hdfs_path_app_db, local_path_app_to_be_crawled, hdfs_path_new=None):
    """
        生成待爬取的包名
    """
    # 将app_install/usage的etl结果中获取的包名和已有的app_db中老的包名
    # 进行比较, 生成新的待爬取包名
    if child_mode == 'NEW':
        df_new = sqc.read.parquet(hdfs_path_new)
        df_db = sqc.read.parquet(hdfs_path_app_db).select('app_name', 'package_name').cache()
        df = df_new.join(df_db, 'package_name', 'left')
        df_to_be_crawled = df.filter("app_name is null").select('package_name')
        print("Num of df_new:%s, Num of df_to_be_crawled:%s" % (df_new.count(), df_to_be_crawled.count()))
        app_name_persist(local_path_app_to_be_crawled, df_to_be_crawled)
    # 待爬取的包名就是app_db中老的包名, 用于更新
    elif child_mode == 'UPGRADE':
        app_name_persist(local_path_app_to_be_crawled, sqc.read.parquet(hdfs_path_app_db).select('package_name'))
    else:
        pass


def app_db_merge_hdfs(sqc, child_mode, local_path_crawl_rst, hdfs_path_app_db, hdfs_path_root):
    """
        将新爬取的app数据和数据仓库中已经存入的app数据进行合并或者更新
    """
    hdfs_path_crawl_rst =  '%s/%s' % (hdfs_path_root, local_path_crawl_rst.split('/')[-1])
    hdfs_delete(hdfs_path_crawl_rst)
    hdfs_transfer(hdfs_path_crawl_rst, local_path_crawl_rst, 'FROM_LOCAL')
    rdd = sc.textFile(hdfs_path_crawl_rst).filter(lambda x : x)
    rdd = rdd.map(lambda x : json_filter(x)).filter(lambda x : x)
    rdd.map(lambda x : Row(package_name=x['package_name'], \
                           install_count=x['install_count'], \
                           score=x['score'], \
                           company=x['company'], \
                           category=x['category'], \
                           app_name=x['app_name'], \
                           icon_url=x['icon_url'], \
                           rank_count=x['rank_count'], \
                           app_type=x['app_type'], \
                           read_more=x['read_more'])).toDF().createOrReplaceTempView('app_db_new')
    df_app_old = sqc.read.parquet(hdfs_path_app_db)
    df_app_new_crawl = sqc.sql("select *, row_number() over (partition by package_name order by package_name) as rr from app_db_new")
    df_app_new_crawl = df_app_new_crawl.filter('rr=1').drop('rr')
    array_col = ['app_name', 'app_type', 'company', 'category', 'icon_url', 'install_count', 'package_name', 'rank_count', 'score', 'read_more']
    # 将新爬取的app_db数据合并进老数据
    if child_mode == 'NEW':
        df_app_new_crawl = df_app_new_crawl.join(df_app_old.select('package_name', lit('1').alias('ref')), 'package_name', 'left')
        df_app_new_crawl = df_app_new_crawl.filter('ref is null').drop('ref')
        df_app_old = df_app_old.select(*array_col)
        df_app_new_crawl = df_app_new_crawl.select(*array_col)
        df_app_new = df_app_new_crawl.union(df_app_old)
        cnt_new = df_app_new.count()
        cnt_old = df_app_old.count()
        if cnt_new >= cnt_old:
            print(cnt_new, cnt_old)
            # 这部分数据用于插入mysql
            df_app_new_crawl.repartition(2).write.mode('overwrite').parquet(hdfs_path_app_db + '_new')
            # 这部分数据用于替换旧数据
            df_app_new.repartition(2).write.mode('overwrite').parquet(hdfs_path_app_db + '_tmp')
            hdfs_delete(hdfs_path_app_db)
            hdfs_move(hdfs_path_app_db + '_tmp', hdfs_path_app_db)
    # 对整个老的app_db数据进行更新
    elif child_mode == 'UPGRADE':
        df_app_old = df_app_old.withColumn('read_more', lit(u'未知'))
        df_app_old = df_app_old.join(df_app_new_crawl.select('package_name', lit('1').alias('ref')), 'package_name', 'left')
        df_app_old_not_upgrade = df_app_old.filter('ref is null').drop('ref')
        udf_str_2_num = udf(lambda x : str_2_num(x))
        df_app_old_not_upgrade = df_app_old_not_upgrade.withColumn('rank_count', udf_str_2_num('rank_count').cast('int')) \
                                                       .withColumn('install_count', udf_str_2_num('install_count').cast('int')) \
                                                       .withColumn('score', udf_str_2_num('score').cast('double'))
        df_app_new_crawl = df_app_new_crawl.withColumn('rank_count', udf_str_2_num('rank_count').cast('int')) \
                                           .withColumn('install_count', udf_str_2_num('install_count').cast('int')) \
                                           .withColumn('score', udf_str_2_num('score').cast('double'))
        df_app_old_upgrade = df_app_new_crawl.select(*array_col).union(df_app_old_not_upgrade.select(*array_col))
        cnt_upgrade = df_app_old_upgrade.count()
        cnt_old = df_app_old.count()
        if cnt_old == cnt_upgrade:
            df_app_old_upgrade.repartition(2).write.mode('overwrite').parquet(hdfs_path_app_db + '_upgrade')
            hdfs_delete(hdfs_path_app_db)
            hdfs_move(hdfs_path_app_db + '_upgrade', hdfs_path_app_db)
        else:
            print("Upgrade app_db num in valid (%s, %s)" % (cnt_old, cnt_upgrade))
            exit(1)
    else:
        pass


def app_db_merge_mysql(sqc, hdfs_path_app_db, record_limit=100, upgrade=False):
    """
        将新爬取的app信息存入mysql
        mysql已经设置避免重复插入策略
    """
    df = sqc.read.parquet(hdfs_path_app_db + '_new') if not upgrade else \
         sqc.read.parquet(hdfs_path_app_db)
    cnt_all = df.count()
    cnt_acc = 0
    if cnt_all > 0:
        config = init_config('%s/conf/config' % crt_root)['data_source_config']['mysql']
        # 将DataFrame按照记录数限制进行分块
        # 1.避免collect的数量过大导致driver out of memory
        # 2.避免单次插入mysql的数据量过大
        if cnt_all > record_limit:
            split_num = int(cnt_all / record_limit) + 1
            # array_data = [df.collect() for df in df.randomSplit([1.0 / split_num for i in range(split_num)], 1)]
            array_data = []
            data_all = df.collect()
            pos = 0
            while pos < len(data_all):
                if pos + record_limit < len(data_all):
                    array_data.append(data_all[pos : pos + record_limit])
                    pos = pos + record_limit
                else:
                    array_data.append(data_all[pos : len(data_all)])
                    break
            del data_all
        else:
            array_data = [df.collect()]
        cnt = 0
        columns = ['package_name', 'app_name', 'company', 'category', 'app_type', 'icon_url', 'install_count', 'rank_count', 'score']
        for data in array_data:
            data = [tuple([str_2_md5(v['package_name'])] + [v[n] for n in columns]) for v in data]
            d_app_db = {'name' : ['uuid'] + columns, \
                        'value' : data, \
                        'table' : config['tlbs']['app_db']}
            start = time.clock()
            add_to_data_source('mysql', d_app_db, False, config, True)
            end = time.clock()
            print("[MYSQL] %s records processed, with %ss consumed! \n [CRT_BLOCK:%s, REST_BLOCKS:%s]" % (len(data), '%.1f' % (end - start), cnt, len(array_data) - cnt))
            cnt += 1


if __name__ == '__main__':
    try:
        mode = sys.argv[1]
        child_mode = sys.argv[2]
        sc, sqc = init_context(mode)
        if mode == 'GET_NEW_PACKAGE':
            array_param = [sqc] + sys.argv[3 : 7]
            get_new_app(*array_param)
        elif mode == 'GENERATE_APP_TO_BE_CRAWLED':
            array_param = [sqc] + (sys.argv[2 : 5] if child_mode == 'UPGRADE' else sys.argv[2 : 6])
            generate_app_to_be_crawled(*array_param)
        elif mode == 'MERGE_TO_HDFS':
            array_param = [sqc] + sys.argv[2 : 6]
            app_db_merge_hdfs(*array_param)
        elif mode == 'MERGE_TO_MYSQL':
            array_param = [sqc, sys.argv[3], 8000, child_mode == 'UPGRADE']
            app_db_merge_mysql(*array_param)
        else:
            pass
        exit(0)
    except Exception as e:
        print(sys.argv)
        traceback.print_exc()
        exit(1)
